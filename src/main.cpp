#include "../inc/celsius.hpp"
#include "../inc/fahrenheit.hpp"
#include "../inc/kelvin.hpp"
#include <iostream>
#include <string>

using namespace std;

int main()
{
	float temperatura;
	int tipoTermometro;

	Celsius temperaturaCelsius;
	Fahrenheit temperaturaFahrenheit;
	Kelvin temperaturaKelvin;

	cout << "Qual termometro deseja utilizar?\n";
	cout << "1 - Celsius\n";
	cout << "2 - Fahrenheit\n";
	cout << "3 - Kelvin\n";
	cin >> tipoTermometro;

	cout << "Digite a temperatura: ";
	cin >> temperatura;	

	switch(tipoTermometro)
	{
		case 1:
			temperaturaCelsius.setTemperatura(temperatura);
			temperaturaCelsius.converterTemperatura();
			break;
		case 2:
			temperaturaFahrenheit.setTemperatura(temperatura);
			temperaturaFahrenheit.converterTemperatura();
			break;
		case 3:
			temperaturaKelvin.setTemperatura(temperatura);
			temperaturaKelvin.converterTemperatura();
			break;
	}

	return 0;
}

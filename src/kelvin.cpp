#include "../inc/kelvin.hpp"
#include <iostream>
#include <string>

using namespace std;

//Construtor
Kelvin::Kelvin()
{
}

//Destrutor
Kelvin::~Kelvin()
{
}

void Kelvin::converterTemperatura()
{
	float celsius, fahrenheit, kelvin;
	
	kelvin = Kelvin::getTemperatura();

	celsius = kelvin - 273;
	fahrenheit = (celsius*9/5)+32;

	cout << "Celsius: " << celsius << endl;
	cout << "Fahrenheit: " << fahrenheit << endl;
}

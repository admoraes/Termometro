#include "../inc/fahrenheit.hpp"
#include <iostream>
#include <string>

using namespace std;

//Construtor
Fahrenheit::Fahrenheit()
{
}

//Destrutor
Fahrenheit::~Fahrenheit()
{
}

void Fahrenheit::converterTemperatura()
{
	float celsius, fahrenheit, kelvin;
	
	fahrenheit = Fahrenheit::getTemperatura();

	celsius = (fahrenheit-32)*5/9;
	kelvin = celsius + 273;

	cout << "Celsius: " << celsius << endl;
	cout << "Kelvin: " << kelvin << endl;
}

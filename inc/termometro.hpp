
#ifndef TERMOMETRO_HPP
#define TERMOMETRO_HPP

class Termometro
{
	private:
	float temperatura;
	
	public:
	Termometro();
	~Termometro();
	void setTemperatura(float temperatura);
	float getTemperatura();
};
#endif
